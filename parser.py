#!/usr/local/bin/python3
import random
import json


filename = "nasdaq.txt"
fh = open(filename, "r")
line = fh.readlines()
lines = [x.rstrip('\n') for x in line]
data = []
for z in range(1,len(lines)-1):
    cells = lines[z].split('|')
    stock = {}
    stock['symbol'] = cells[0]
    stock['name'] = cells[1]
    data.append(stock)
fh.close()
filename = "nyse.txt"
fh = open(filename, "r")
line = fh.readlines()
lines = [x.rstrip('\n') for x in line]
for z in range(1,len(lines)-1):
    cells = lines[z].split('|')
    stock = {}
    stock['symbol'] = cells[0]
    stock['name'] = cells[1]
    data.append(stock)
fh.close()
json_data = json.dumps(data)
filename = "output_json.txt"
fh = open(filename, "w")
fh.write(json_data)
print(json_data)