package com.dwl.server;


import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import etcs.Performance;
import persistence.CRUDService.NotFoundException;
import persistence.DBDriver;
import persistence.Strategy;
import strategies.StrategyRunner;
import web.ConvertJsonToJPA;
import web.StatusJson;
import web.StrategyJson;

/**
 * @author Lelen Abeywardena
 *
 */
@CrossOrigin(origins = "http://localhost:4200")
@Controller
public class DWLController {
	private static final Logger LOGGER = Logger.getLogger( DWLController.class.getName() );
	
	private int createStrategy(StrategyJson strategyJson) {
		
		Strategy strategy = ConvertJsonToJPA.convertStrategy(strategyJson);
		DBDriver.getInstance().insertStrategy(strategy);
		
		LOGGER.log(Level.INFO,strategy.toString());
		return strategy.getId();
	}
	
	private void startStrategy(int ID) throws NotFoundException {
		
		Strategy strategy = DBDriver.getInstance().getStrategy(ID);
		StrategyRunner.getInstance().enqueueStrategy(strategy);
		
	}
	
	private void stopStrategy(int ID) {
		StrategyRunner.getInstance().dequeueStrategy(ID);
	}
	
	private StatusJson getStatus(int ID) throws NotFoundException {
		return StrategyRunner.getInstance().getStatus(ID);
	}
	
	private ArrayList<StatusJson> getActiveStrategies(){
		return StrategyRunner.getInstance().getActiveStrategies();
	}
	
	private ArrayList<StatusJson> getStrategies(){
		return StatusJson.getStrategies();
	}
	
	private ArrayList<Performance> getPerformance(){
		return DBDriver.getInstance().getPerformance();
	}
	

    @RequestMapping("/")
    public String handler(Model model) {
        model.addAttribute("msg", "Spring Boot web app, JAR packaging, JSP views");
        return "index";
    }

    
    /**
    Creates a new order.
    */
    @RequestMapping(method=RequestMethod.POST, value="/Create")
    @ResponseStatus(value = HttpStatus.CREATED)
    @ResponseBody 
    public String create (@RequestBody @Valid StrategyJson strategyJson)
    {
        return String.valueOf(createStrategy(strategyJson));
    }
    
    @RequestMapping(method=RequestMethod.PUT, value="/Start/{ID}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @ResponseBody
    public void start (@PathVariable("ID") int ID) throws NotFoundException
    {
        startStrategy(ID);
    }
    
    @RequestMapping(method=RequestMethod.PUT, value="/Stop/{ID}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @ResponseBody
    public void stop (@PathVariable("ID") int ID)
    {
        stopStrategy(ID);
    }
    
    @RequestMapping(method=RequestMethod.GET, value="/Status/{ID}")
    @ResponseBody
    public StatusJson checkStatus (@PathVariable("ID") int ID) throws NotFoundException
    {
    	return getStatus(ID);
    }
    
    @RequestMapping(method=RequestMethod.GET, value="/Strategies/Active")
    @ResponseBody
    public ArrayList<StatusJson> activeStrategies () 
    {
    	return getActiveStrategies();
    }
    
    @RequestMapping(method=RequestMethod.GET, value="/Strategies")
    @ResponseBody
    public ArrayList<StatusJson> checkStatus () 
    {
    	return getStrategies();
    }
    
    @RequestMapping(method=RequestMethod.GET, value="/Performance")
    @ResponseBody
    public ArrayList<Performance> checkPerformance () 
    {
    	return getPerformance();
    }

}
