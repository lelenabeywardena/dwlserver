package com.dwl.server;

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.support.GenericXmlApplicationContext;

import etcs.TimedExecuter;
/**
 * @author Lelen Abeywardena
 */
@SpringBootApplication
public class DwlserverApplication {
	private static final Logger LOGGER = Logger.getLogger(DwlserverApplication.class.getName());
	public static void main(String[] args) {
		SpringApplication.run(DwlserverApplication.class, args);
		Timer timer = new Timer();
		TimerTask task = new TimedExecuter();
		LOGGER.log(Level.INFO,"dwlmain start");
		timer.schedule(task, 15000, 15000);
		GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
		ctx.load("classpath:broker-config.xml");
		ctx.refresh();
	}
}
