package etcs;


/**
 * @author Wei Du
 */

public class Performance {

	public int id;
	public String gain;
	public Performance(int id, String gain) {
		super();
		this.id = id;
		this.gain = gain;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getGain() {
		return gain;
	}
	public void setGain(String gain) {
		this.gain = gain;
	}

}
