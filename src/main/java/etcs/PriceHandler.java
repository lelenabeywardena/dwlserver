package etcs;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

import org.springframework.web.client.RestTemplate;
/**
 * @author Lelen Abeywardena
 * @author Wei Du
 */
public class PriceHandler {
	

	public Price parse(String line) {

		String[] cell = line.split(",");
		Price tmp = new Price(cell[0], Double.parseDouble(cell[1]), Double.parseDouble(cell[2]),
				Double.parseDouble(cell[3]), Double.parseDouble(cell[4]), Integer.parseInt(cell[5]));
		return tmp;
	}

	public Queue<Price> download(String ticker, int periods) {
		Queue<Price> q = new LinkedList<>();
		RestTemplate template = new RestTemplate();
		;
		String reponse = template.getForObject(
				("http://incanada1.conygre.com:9080/prices/" + ticker + "?periods=" + periods), String.class);
		Scanner scanner = new Scanner(reponse);
		String line = "";
		int flag = 0;
		while (scanner.hasNextLine()) {
			line = scanner.nextLine();
			if (flag != 0) {
				//System.out.println(line);
				Price tmp = parse(line);
				q.add(tmp);
			}
			flag = 1;
			// process the line
		}
		scanner.close();

		return q;
	}

	public Queue<Price> update(String ticker, int periods, Queue<Price> old) {
		Queue<Price> q = new LinkedList<Price>();
		String timestamp = "";
		int flag = 0;
		int readflag = 0;

		RestTemplate template = new RestTemplate();
		;
		String reponse = template.getForObject(
				("http://incanada1.conygre.com:9080/prices/" + ticker + "?periods=" + periods), String.class);
		Scanner scanner = new Scanner(reponse);
		String line = "";
		while (old.size() > 0) {
			if (!(old.size() > 1)) {
				Price tmp = old.poll();
				timestamp = tmp.timestamp;
				q.add(tmp);
			} else {
				q.add(old.poll());
			}
		}
		old.clear();
		while (scanner.hasNextLine()) {
			line = scanner.nextLine();
			if (flag != 0) {
				//System.out.println(line);
				Price tmp = parse(line);
				if (readflag == 1) {
					q.add(tmp);
				}
				if (tmp.timestamp.equals(timestamp)) {
					readflag = 1;
				}
			}
			flag = 1;
			// process the line
		}
		scanner.close();

		return q;
	}
}
