package etcs;

import java.util.Iterator;
import java.util.TimerTask;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import strategies.StrategyExceptions.ExitOverThreshold;
import strategies.StrategyRunner;
import strategies.TradingStrategy;

/**
 * @author Lelen Abeywardena
 * @author Wei Du
 */

public class TimedExecuter extends TimerTask {
	private static final Logger LOGGER = Logger.getLogger(TimedExecuter.class.getName());
	ReadWriteLock lock = new ReentrantReadWriteLock();

	@Override
	public void run() {
		StrategyRunner runner = StrategyRunner.getInstance();
		
		if(!runner.getRunningStrategies().isEmpty()) {
			TradingStrategy t;
			runner.updatePrices();
			lock.readLock().lock();
			Iterator<TradingStrategy> iter = runner.getRunningStrategies().iterator();
			while (iter.hasNext()) {
				t = iter.next();
				try {
					runner.runStrategy(t);
				} catch (ExitOverThreshold e) {
					LOGGER.log(Level.INFO,"Strategy "+t.tradingStrategy+" hit exit threshold.", e);
					t.tradingStrategy.setActive(false);
					iter.remove();
				}
			}
			
			lock.readLock().lock();
		}
		LOGGER.log(Level.INFO,"new loop");
	}

}
