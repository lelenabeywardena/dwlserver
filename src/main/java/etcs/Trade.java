package etcs;

import persistence.Strategy;

/**
 * @author Wei Du
 */

public class Trade {
	String timestamp ;
	String ticker;
	String type;
	double Price;
	int eSize;
	String result;
	Strategy strategy;
	
	public Trade(String timestamp, String ticker, String type, double price, int eSize, String result,
			Strategy strategy) {
		super();
		this.timestamp = timestamp;
		this.ticker = ticker;
		this.type = type;
		Price = price;
		this.eSize = eSize;
		this.result = result;
		this.strategy = strategy;
	}

	
	public Strategy getStrategy() {
		return strategy;
	}


	public void setStrategy(Strategy strategy) {
		this.strategy = strategy;
	}


	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getTicker() {
		return ticker;
	}
	public void setTicker(String ticker) {
		this.ticker = ticker;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public double getPrice() {
		return Price;
	}
	public void setPrice(double price) {
		Price = price;
	}
	public int geteSize() {
		return eSize;
	}
	public void seteSize(int eSize) {
		this.eSize = eSize;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String toString() {
		return "Strategy "+this.strategy.getId()+" buy?" + this.type + " " + this.result  +" size of"+ this.eSize + "\n" + this.ticker + "for "+this.Price+" at " + this.timestamp;
	}

}
