package messaging;

import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import etcs.Trade;
import persistence.DBDriver;
import persistence.Strategy;
import persistence.Transaction;
import strategies.StrategyRunner;
import strategies.TradingStrategy;
/**
 * @author Lelen Abeywardena
 * @author Wei Du
 */
public class BrokerListener implements MessageListener{
	private static final Logger LOGGER = Logger.getLogger(BrokerListener.class.getName());
	public void onMessage(Message m) {
		LOGGER.log(Level.INFO,"In Listener");
		TextMessage message = (TextMessage) m;
		try {
			LOGGER.log(Level.INFO,"correlationID is :"+message.getJMSCorrelationID());
			verify(message.getText(),message.getJMSCorrelationID());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void verify(String msg,String id) {
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder builder;
//	    int value = 0;
		try {
			builder = factory.newDocumentBuilder();
			InputSource is = new InputSource(new StringReader(msg));
			Document doc = builder.parse(is);
			doc.getDocumentElement().normalize();
//			Element eResult = (Element) doc.getElementsByTagName("result").item(0);
//			Element ePrice = (Element) doc.getElementsByTagName("price").item(0);
//			Element eSize = (Element) doc.getElementsByTagName("size").item(0);
//			Element eDate = (Element) doc.getElementsByTagName("whenAsDate").item(0);
//			Element eStock = (Element) doc.getElementsByTagName("stock").item(0);
//			Element eBuy = (Element) doc.getElementsByTagName("buy").item(0);
			TradingStrategy ts = StrategyRunner.getInstance().getRunningStrategy(Integer.parseInt(id));
			Strategy fetchedStrategy = ts.tradingStrategy;
			if(fetchedStrategy!=null) { //fixed null pointed issue when old message in queue
			Trade tmp = new Trade(doc.getElementsByTagName("whenAsDate").item(0).getTextContent(), 
					doc.getElementsByTagName("stock").item(0).getTextContent(), 
					doc.getElementsByTagName("buy").item(0).getTextContent(),
					Double.parseDouble(doc.getElementsByTagName("price").item(0).getTextContent()),
					Integer.parseInt(doc.getElementsByTagName("size").item(0).getTextContent()),
					doc.getElementsByTagName("result").item(0).getTextContent(),
					fetchedStrategy);
			LOGGER.log(Level.INFO,tmp.toString());
			push_transaction(tmp, ts);
			}
		} catch (ParserConfigurationException e) {
			// TODO catch error
			e.printStackTrace();

		}
		catch(Exception e) {
			e.printStackTrace();

		}		
	}
	public boolean push_transaction(Trade trade, TradingStrategy ts) {
		long time = System.currentTimeMillis()/1000L ;
		
		if(trade.getResult().equals("FILLED")) {
			if(trade.getType().equals("false")) {
				//2 is buy, 1 is sell
				ts.twoMA.setStatus(1);
				ts.twoMA.setPositionPrice(trade.getPrice());
				Transaction tmp = new Transaction(1, trade.getPrice(), trade.geteSize(), trade.getStrategy(), (int)time);
				DBDriver.getInstance().insertTransaction(tmp);
				LOGGER.log(Level.INFO,"Transaction recorded in database.");
			}
			else {
				//2 is buy, 1 is sell
				ts.twoMA.setStatus(2);
				ts.twoMA.setPositionPrice(trade.getPrice());
				Transaction tmp = new Transaction(2, trade.getPrice(), trade.geteSize(), trade.getStrategy(), (int)time);
				DBDriver.getInstance().insertTransaction(tmp);
				LOGGER.log(Level.INFO,"Transaction recorded in database.");

			}
		}
		else if(trade.getResult().equals("PARTIALLY_FILLED")) {
			if(trade.getType().equals("false")) {
				//0 is buy, 1 is sell
				Transaction tmp = new Transaction(1, trade.getPrice(), trade.geteSize(), trade.getStrategy(), (int)time);
				DBDriver.getInstance().insertTransaction(tmp);
				LOGGER.log(Level.INFO,"Partial Filled. Transaction recorded in database.");
				BrokerMessageHandler sellhandler = BrokerMessageHandler.getInstance();
				//int target = (int) ((trade.getStrategy().getPar4())/trade.getPrice()) - trade.geteSize();
				int target = trade.getStrategy().getPar4() - trade.geteSize();
				sellhandler.sell(trade.getPrice(), target, trade.getTicker(), trade.getStrategy().getId());
				LOGGER.log(Level.INFO,"sell reattempt.");
			}
			else {
				//0 is buy, 1 is sell
				Transaction tmp = new Transaction(0, trade.getPrice(), trade.geteSize(), trade.getStrategy(), (int)time);
				DBDriver.getInstance().insertTransaction(tmp);
				LOGGER.log(Level.INFO,"Partial Filled. Transaction recorded in database.");
				BrokerMessageHandler buyhandler = BrokerMessageHandler.getInstance();
				int target = trade.getStrategy().getPar4() - trade.geteSize();
				buyhandler.buy(trade.getPrice(), target, trade.getTicker(), trade.getStrategy().getId());
				LOGGER.log(Level.INFO,"buy reattempt.");			}
			
		}
		else if(trade.getResult().equals("REJECTED")) {
			if(trade.getType().equals("false")) {
				LOGGER.log(Level.WARNING,"REJECTED! Transaction NOT recorded in database.");
				BrokerMessageHandler sellhandler = BrokerMessageHandler.getInstance();
				int target = trade.getStrategy().getPar4();
				sellhandler.buy(trade.getPrice(), target, trade.getTicker(), trade.getStrategy().getId());
				LOGGER.log(Level.INFO,"sell reattempt.");
			}
			else {
				LOGGER.log(Level.WARNING,"REJECTED! Transaction NOT recorded in database.");
				BrokerMessageHandler buyhandler = BrokerMessageHandler.getInstance();
				int target = trade.getStrategy().getPar4();
				buyhandler.buy(trade.getPrice(), target, trade.getTicker(), trade.getStrategy().getId());
				LOGGER.log(Level.INFO,"buy reattempt.");
			}
		return false;
		}
		return false;
		
	}

}
