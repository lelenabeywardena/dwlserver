package messaging;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

/**
 * @author Lelen Abeywardena
 * @author Wei Du
 */

public class BrokerMessageHandler {
	private ApplicationContext context;
	private JmsTemplate jmsTemplate;
	private Destination destination;
	private Destination replyTo;
	private static BrokerMessageHandler mmh = null;

	private BrokerMessageHandler() {
		context = new ClassPathXmlApplicationContext("broker-config.xml");
		destination = context.getBean("destination", Destination.class);
		replyTo = context.getBean("replyTo", Destination.class);
		jmsTemplate = (JmsTemplate) context.getBean("messageSender");
	}

	public static BrokerMessageHandler getInstance() {
		if (mmh == null) {
			mmh = new BrokerMessageHandler();
		}
		return mmh;
	}

	public static void main(String[] args) {
		BrokerMessageHandler mmh = new BrokerMessageHandler();
		mmh.run();
	}

	private void run() {
		// testing fuction
		// makeMessage(destination);
		sell(118, 10000, "C", 15);
		System.out.println("Message Sent to JMS Queue:- ");

	}

	public String buy(double price, int size, String symbol, int id) {
		String time = OffsetDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		String builder = "<trade>\n" + "  <buy>true</buy>\n" + "  <id>" + id + "</id>\n" + "  <price>" + price
				+ "</price>\n" + "  <size>" + size + "</size>\n" + "  <stock>" + symbol + "</stock>\n"
				+ "  <whenAsDate>" + time + "</whenAsDate>\n" + "</trade>";
		jmsTemplate.send(destination, new MessageCreator() {
			public Message createMessage(Session session) throws JMSException {
				TextMessage message = session.createTextMessage();
				message.setText(builder);
				message.setJMSReplyTo(replyTo);
				message.setJMSCorrelationID("" + id);
				return message;
			}
		});
		return builder;
	}

	public String sell(double price, int size, String symbol, int id) {
		String time = OffsetDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		String builder = "<trade>\n" + "  <buy>false</buy>\n" + "  <id>" + id + "</id>\n" + "  <price>" + price
				+ "</price>\n" + "  <size>" + size + "</size>\n" + "  <stock>" + symbol + "</stock>\n"
				+ "  <whenAsDate>" + time + "</whenAsDate>\n" + "</trade>";
		jmsTemplate.send(destination, new MessageCreator() {
			public Message createMessage(Session session) throws JMSException {
				TextMessage message = session.createTextMessage();
				message.setText(builder);
				message.setJMSReplyTo(replyTo);
				message.setJMSCorrelationID("" + id);
				return message;
			}
		});
		return builder;
	}
	/*
	 * private void makeMessage(final Destination destination) {
	 * jmsTemplate.send(destination, new MessageCreator() { public Message
	 * createMessage(Session session) throws JMSException { TextMessage message =
	 * session.createTextMessage(); message.setText(buy(12.3, 10000, "AAPL", 1));
	 * message.setJMSReplyTo(replyTo); message.setJMSCorrelationID("9991235");
	 * return message; } });
	 * 
	 * }
	 */
}
