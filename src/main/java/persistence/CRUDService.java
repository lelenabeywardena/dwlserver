package persistence;

import javax.transaction.Transactional;


/**
 * @author Lelen Abeywardena
 */
public interface CRUDService<T> {

	public static class NotFoundException extends Exception {
		private Class<?> type;
		private int ID;

		public NotFoundException(Class<?> type, int ID) {
			super("No " + type.getSimpleName() + " found with ID " + ID + ".");

			this.type = type;
			this.ID = ID;
		}

		public Class<?> getType() {
			return type;
		}

		public int getID() {
			return ID;
		}
	}
	
	@Transactional
    public T getByID (int ID)
        throws NotFoundException;
	
	@Transactional
    public T add (T newObject);
}
