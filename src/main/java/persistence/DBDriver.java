package persistence;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.persistence.EntityManagerFactory;

import etcs.Performance;
import persistence.CRUDService.NotFoundException;



/**
 * @author Lelen Abeywardena
 *
 */
public class DBDriver {
	private static DBDriver dbdriver = null;
	private EntityManagerFactory factory;
	private EntityManager em;

	private DBDriver() {
		factory = Persistence.createEntityManagerFactory("DWL");
		em = factory.createEntityManager();
	}
	
	public static DBDriver getInstance() {
		if(dbdriver==null) {
			dbdriver = new DBDriver();
		}
		return dbdriver;
	}
	
	public void insertStrategy(Strategy strategy) {
		
		insertIntoDatabase(strategy);
	}
	public void insertTransaction(Transaction transaction) {
		
		insertIntoDatabase(transaction);
	}
	private void insertIntoDatabase(Object obj) {
		try {
			if(!em.getTransaction().isActive()) {
				em.getTransaction().begin();
			}
			em.persist(obj);
			em.getTransaction().commit();
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Failed to add new objects to database");
		}
		
	}

	public Strategy getStrategy(int ID) throws NotFoundException{
		Strategy s = em.find(Strategy.class, ID);
		if(s!=null) {
			return s;
		}
		else {
			throw new NotFoundException(Strategy.class, ID);
		}
		 

	}
	
	public ArrayList<Transaction> getTrades(Strategy s){
		Query query = em.createQuery( "Select t from Transaction as t where strategy = ?1" , Transaction.class);
		query.setParameter( 1, s );
		return (ArrayList<Transaction>) query.getResultList();
	}
	
	public ArrayList<Strategy> getAllStrategies(){
		Query query = em.createQuery( "Select s from Strategy as s" , Strategy.class);
		//query.setParameter( 1, s );
		return (ArrayList<Strategy>) query.getResultList();
	}
	public ArrayList<Performance> getPerformance(){
		Query query = em.createNativeQuery( "SELECT SUM(CASE WHEN POSITION = 1 THEN (-PRICE*QUANTITY) ELSE (PRICE*QUANTITY) END)AS COST, STRATEGY  FROM TRANSACTION \n" + 
				"GROUP BY STRATEGY");
		List<Object[]> values = query.getResultList();	
		ArrayList<Performance> paresedvalues = new ArrayList<Performance>();
		for (Object[] a : values) {
			if(a[0].toString().contains(".")) {
				Performance tmp = new Performance(Integer.parseInt(a[1].toString()),a[0].toString().substring(0, a[0].toString().indexOf(".")+3));
				paresedvalues.add(tmp);
			}
			else {
				Performance tmp = new Performance(Integer.parseInt(a[1].toString()),a[0].toString());
				paresedvalues.add(tmp);
			}
		}
		return paresedvalues;
	}
	public void close() {
		factory.close();
	}


}