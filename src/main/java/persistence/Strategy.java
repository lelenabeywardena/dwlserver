package persistence;
import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Lelen Abeywardena
 * @author Wei Du
 */

@Entity
@Table(name = "STRATEGY")
public class Strategy implements Serializable {
	
	private static final long serialVersionUID = 506682728744849677L;

	@Id @GeneratedValue(strategy = GenerationType.TABLE) @Column(name="id")
    private int id;
   
	@Column(name = "TICKER")
    private String ticker;
	
	@Column(name = "TIMECREATED")
	private int timecreated;
	
	@Column(name = "PAR1")
    private int par1;
	
	@Column(name="PAR2")
    private int par2;
	
	@Column(name="PAR3")
    private int par3;
	
	@Column(name="PAR4")
    private int par4;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "type")
	private StrategyType type;
	
	@Column(name = "active")
	private boolean active;
	
    
    public boolean isActive() {
		return active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}


	public Strategy() {
    	
    }
    

	public Strategy(String ticker, int par1, int par2, int par3, int par4, StrategyType type, int timecreated) {
		super();
		this.ticker = ticker;
		this.timecreated = timecreated;
		this.par1 = par1;
		this.par2 = par2;
		this.par3 = par3;
		this.par4 = par4;
		setStrategyType(type);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public int getTimecreated() {
		return timecreated;
	}

	public void setTimecreated(int timecreated) {
		this.timecreated = timecreated;
	}

	public int getPar1() {
		return par1;
	}

	public void setPar1(int par1) {
		this.par1 = par1;
	}
	
	public int getPar2() {
		return par2;
	}

	public void setPar2(int par2) {
		this.par2 = par2;
	}

	public int getPar3() {
		return par3;
	}

	public void setPar3(int par3) {
		this.par3 = par3;
	}
	
	public int getPar4() {
		return par4;
	}

	public void setPar4(int par4) {
		this.par4 = par4;
	}

	
	public StrategyType getStrategyType() {
		return type;
	}
	
	public void setStrategyType(StrategyType st) {
		this.type = st;
	}


	@Override
	public String toString() {
		return "Strategy [id=" + id + ", ticker=" + ticker + ", timecreated=" + timecreated + ", par1=" + par1
				+ ", par2=" + par2 + ", par3=" + par3 + ", par4=" + par4 + ", type=" + type + ", active=" + active
				+ "]";
	}

}
