package persistence;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Lelen Abeywardena
 * @author Wei Du
 */

@Entity
@Table(name = "STRATEGYTYPE")
public class StrategyType implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 9116781690890656153L;

	@Id 
	@GeneratedValue(strategy = GenerationType.TABLE) @Column(name="strategyId")
	private int strategyId;
	
	@Column(name = "name")
	private String name;
	
	public StrategyType() {
		
	}
	/**
	 * @param id
	 * @param name
	 */
	public StrategyType(String name) {
		super();
		setName(name);
	}
	public int getId() {
		return strategyId;
	}
	
	public void setId(int id) {
		this.strategyId = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "StrategyType [strategyId=" + strategyId + ", name=" + name + "]";
	}
	
}
