package persistence;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * @author Lelen Abeywardena
 * @author Wei Du
 */

@Entity
@Table(name = "TRANSACTION")
public class Transaction implements Serializable{

	private static final long serialVersionUID = -6697568235155205658L;
	@Id 
	@GeneratedValue(strategy = GenerationType.TABLE) @Column(name="id")
	private int id;
	private int position;
	private Double price;
	private int quantity;
	//@Column(length = 500)
	@ManyToOne
	@JoinColumn(name = "strategy")
	private Strategy strategy;
	private int timeplaced;
	
	public Transaction() {
		
	}
	/**
	 * @param id
	 * @param position
	 * @param price
	 * @param quantity
	 * @param strategy
	 * @param symbol
	 * @param timeplaced
	 */
	public Transaction(int position, Double price, int quantity, Strategy strategy, int timeplaced) {
		super();
		setPosition(position);
		setPrice(price);
		setQuantity(quantity);
		setStrategy(strategy);
		setTimeplaced(timeplaced);
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	@ManyToOne
	@JoinColumn(name = "id")
	public Strategy getStrategy() {
		return strategy;
	}
	public void setStrategy(Strategy strategy) {
		this.strategy = strategy;
	}
	public int getTimeplaced() {
		return timeplaced;
	}
	public void setTimeplaced(int timeplaced) {
		this.timeplaced = timeplaced;
	}
	
	
	
	
}
