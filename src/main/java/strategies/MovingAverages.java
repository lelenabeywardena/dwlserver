package strategies;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

import etcs.Price;
/**
 * @author Dan Deng
 * @author Wei Du
 */


public class MovingAverages {
	private static final Logger LOGGER = Logger.getLogger( MovingAverages.class.getName() );

    int period;
    Queue<Price> dataStorage = new LinkedList<Price>();
    
    public MovingAverages(int initialPeriod){
        period = initialPeriod;
    }
    
    public MovingAverages(Queue<Price> initialData, int initialPeriod) {
        dataStorage = initialData;
        period = initialPeriod;
        
    }    
//    public double addData(double data) throws Exception {
//        if (dataStorage.size() < period) {
//            dataStorage.add (data);
//            
//            if (dataStorage.size () >= period) {
//                try {
//                	return calcMA();
//                } catch (Exception e) {
//                	throw e;
//                }
//            }
//            else {
//                return -1;
//            }
//        }
//        else {
//            dataStorage.remove();
//            dataStorage.add (data);
//            try {
//            	return calcMA();
//            } catch (Exception e) {
//            	throw e;
//            }
//        }
//    }    
    public double calcMA() throws Exception {
            Double count = (double) 0;
            for (int i=0; i<this.period; i++) {
            	try {
            	double average = (((LinkedList<Price>) dataStorage).get(i).low + ((LinkedList<Price>) dataStorage).get(i).high)/2;
                count += average;
            	} catch (Exception e) {
            		LOGGER.log(Level.SEVERE, "Average has been asked too soon");
                    throw e;
            	}
            }
            LOGGER.log(Level.INFO,Double.toString((Math.round((count/period) * 100.0)/100.0)));
            return Math.round((count/period) * 100.0)/100.0;

    }
}

