package strategies;

public interface StrategyExceptions {
	
	public static class ExitOverThreshold extends Exception {

		public ExitOverThreshold() {
			super();
	}
	}
		
		public static class NotEnoughData extends Exception {

			public NotEnoughData() {
				super();
		}
}
}
