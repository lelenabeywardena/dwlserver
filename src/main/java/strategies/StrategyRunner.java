package strategies;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import etcs.Price;
import etcs.PriceHandler;
import persistence.CRUDService.NotFoundException;
import persistence.DBDriver;
import persistence.Strategy;
import persistence.Transaction;
import strategies.StrategyExceptions.ExitOverThreshold;
import web.ConvertJPAToJson;
import web.StatusJson;

/**
 * @author Lelen Abeywardena
 *
 */
public class StrategyRunner {
	private static final Logger LOGGER = Logger.getLogger(StrategyRunner.class.getName());
	ReadWriteLock lock = new ReentrantReadWriteLock();
	private static StrategyRunner instance = null;
	private PriceHandler priceHandler;
	private ArrayList<TradingStrategy> runningStrategies;
	private HashMap<String, Queue<Price>> prices;

	protected StrategyRunner() {
		priceHandler = new PriceHandler();
		runningStrategies = new ArrayList<TradingStrategy>();
		prices = new HashMap<String, Queue<Price>>();
	}

	public static StrategyRunner getInstance() {
		if (instance == null) {
			instance = new StrategyRunner();
		}
		return instance;
	}

	public void updatePrices() {
		for (String k : prices.keySet()) {
			prices.put(k, priceHandler.download(k, 50));
		}
	}

	public ArrayList<TradingStrategy> getRunningStrategies() {
		return runningStrategies;
	}

	public TradingStrategy getRunningStrategy(int ID) {
		for (TradingStrategy s : runningStrategies) {
			if (s.tradingStrategy.getId() == ID) {
				return s;
			}
		}
		return null;
	}

	public void enqueueStrategy(Strategy strategy) {
		lock.writeLock().lock();
		Queue<Price> priceData = priceHandler.download(strategy.getTicker(), 50);

		if (!prices.containsKey(strategy.getTicker())) {
			prices.put(strategy.getTicker(), priceData);
		}
		strategy.setActive(true);

		TradingStrategy t = new TradingStrategy(strategy, priceData, strategy.getPar1(), strategy.getPar2());
		runningStrategies.add(t);
		LOGGER.log(Level.INFO, "strategy in queue");
		lock.writeLock().unlock();

	}

	public void dequeueStrategy(int ID) {
		lock.writeLock().lock();
		Iterator<TradingStrategy> iter = runningStrategies.iterator();
		while (iter.hasNext()) {
			TradingStrategy t = iter.next();
			if (t.tradingStrategy.getId() == ID) {

				t.tradingStrategy.setActive(false);
				iter.remove();

			}
		}
		lock.writeLock().unlock();
	}

	public StatusJson getStatus(int ID) throws NotFoundException {

		lock.readLock().lock();
		Iterator<TradingStrategy> iter = runningStrategies.iterator();
		while (iter.hasNext()) {
			TradingStrategy t = iter.next();
			if (t.tradingStrategy.getId() == ID) {
				return ConvertJPAToJson.createStatusJson(t.tradingStrategy,
						DBDriver.getInstance().getTrades(t.tradingStrategy));
			}
		}
		lock.readLock().unlock();
		Strategy ss = DBDriver.getInstance().getStrategy(ID);
		ArrayList<Transaction> trades = DBDriver.getInstance().getTrades(ss);

		return ConvertJPAToJson.createStatusJson(ss, trades);
	}

	public ArrayList<StatusJson> getActiveStrategies() {

		ArrayList<StatusJson> activeStrategies = new ArrayList<StatusJson>();
		lock.readLock().lock();
		Iterator<TradingStrategy> iter = runningStrategies.iterator();
		while (iter.hasNext()) {
			TradingStrategy t = iter.next();
			activeStrategies.add(ConvertJPAToJson.createStatusJson(t.tradingStrategy,
					DBDriver.getInstance().getTrades(t.tradingStrategy)));

		}
		lock.readLock().unlock();

		return activeStrategies;

	}

	public void runStrategy(TradingStrategy t) throws ExitOverThreshold {
		lock.writeLock().lock();
		LOGGER.log(Level.INFO, "running strategy: " + t.tradingStrategy.getId());
		t.execute(prices.get(t.tradingStrategy.getTicker()));
		lock.writeLock().unlock();
	}

}
