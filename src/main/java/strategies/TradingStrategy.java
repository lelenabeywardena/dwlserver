package strategies;

import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

import etcs.Price;
import messaging.BrokerMessageHandler;
import persistence.Strategy;
import strategies.StrategyExceptions.ExitOverThreshold;
import strategies.StrategyExceptions.NotEnoughData;

/**
 * @author Dan Deng
 */
enum Decision {
	BUY, SELL, HOLD, ERROR
}

public class TradingStrategy {
	private static final Logger LOGGER = Logger.getLogger(TradingStrategy.class.getName());
	public Strategy tradingStrategy;
	public Queue<Price> prices;
	public int shortDuration;
	public int longDuration;
	public TwoMovingAveragesStrategy twoMA;

	public Decision decision;

	public TradingStrategy(Strategy tradingStrategy, Queue<Price> prices, int shortDuration, int longDuration) {
		this.setUpStrategy(tradingStrategy, prices, shortDuration, longDuration);
	}

	// set up constructor for Two MA
	public void setUpStrategy(Strategy tradingStrategy, Queue<Price> prices, int shortDuration, int longDuration) {
		this.prices = prices;
		this.tradingStrategy = tradingStrategy;

		if (this.tradingStrategy.getStrategyType().getName().toUpperCase().equals("TWOMA")) {
			this.shortDuration = shortDuration;
			this.longDuration = longDuration;
			this.twoMA = new TwoMovingAveragesStrategy();
		} else {
			LOGGER.log(Level.WARNING, "Parameters do not match the requirement for a Two MA strategy");
		}
	}

	public Decision executeTwoMA(Queue<Price> price) throws ExitOverThreshold {
		LOGGER.log(Level.INFO, "testing");
		this.twoMA.setShortPrices(price, this.shortDuration);
		this.twoMA.setLongPrices(price, this.longDuration);
		this.twoMA.setThreshold(this.tradingStrategy.getPar3() / 100.0);
		Price lastElement = new Price("", 1, 1, 1, 1, 1);

		for (Price x : price) {
			lastElement = x;
		}
		try {
			this.decision = this.twoMA.make_decision(lastElement.close);
			
		} catch (NotEnoughData e) {
			LOGGER.log(Level.INFO, "Not Enough Prices", e);
		} 
		LOGGER.log(Level.INFO, this.decision.toString());
		return this.decision;

	}

	public void execute(Queue<Price> price) throws ExitOverThreshold {
		Decision decision = Decision.HOLD;
		if (this.tradingStrategy.getStrategyType().getName().equals("TWOMA")) {
			decision = executeTwoMA(price);
		}
		if (decision.equals(Decision.BUY)) {
			BrokerMessageHandler buyhandler = BrokerMessageHandler.getInstance();
			// par format
			// par4 money
			// par3 thread loss
			// par2 long moving average
			// par1 short moving average
			Price lastElement = new Price("", 1, 1, 1, 1, 1);

			for (Price x : price) {
				lastElement = x;
			}
			// Future implementation
			// int value = (int) (tradingStrategy.getPar4() / lastElement.close);
			buyhandler.buy(lastElement.close, tradingStrategy.getPar4(), tradingStrategy.getTicker(),
					tradingStrategy.getId());
			LOGGER.log(Level.INFO, "buy captured in execute");
		} else if (decision.equals(Decision.SELL)) {
			BrokerMessageHandler buyhandler = BrokerMessageHandler.getInstance();
			Price lastElement = new Price("", 1, 1, 1, 1, 1);

			for (Price x : price) {
				lastElement = x;
			}
			// Future implementation
			// int value = (int) (tradingStrategy.getPar4() / lastElement.close);
			buyhandler.sell(lastElement.close, tradingStrategy.getPar4(), tradingStrategy.getTicker(),
					tradingStrategy.getId());
			LOGGER.log(Level.INFO, "sell captured in execute");
		} else if (decision.equals(Decision.HOLD)) {
			LOGGER.log(Level.INFO, "hold captured in execute");
		}
	}
}
