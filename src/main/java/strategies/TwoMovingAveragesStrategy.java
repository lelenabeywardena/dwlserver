package strategies;

import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

import etcs.Price;

/**
 * @author Dan Deng
 * @author Jack Guo
 */

public class TwoMovingAveragesStrategy implements StrategyExceptions {
	private static final Logger LOGGER = Logger.getLogger(TradingStrategy.class.getName());
    public MovingAverages short_prices;
    public MovingAverages long_prices;
    public double long_average;
    public double short_average;
    // 0 = neutral, 1=short, 2=long
    private int status;
    private double positionPrice;
    private double threshold;
    
    public double getThreshold() {
		return threshold;
	}

	public void setThreshold(double threshold) {
		this.threshold = threshold;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
    
    public double getPositionPrice() {
		return positionPrice;
	}

	public void setPositionPrice(double d) {
		this.positionPrice = d;
	}

	//Empty constructor to reset status
    public TwoMovingAveragesStrategy(){
        status = 0;
    }
    
    //Set short prices with initial list of prices and period
    public void setShortPrices(Queue<Price> ini, int period){
        short_prices = new MovingAverages(ini, period); 
        try {
			short_average = short_prices.calcMA ();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    //Set short prices with no initial list of prices
    public void setShortPrices(int period){
        short_prices = new MovingAverages(period); 
    }
    
    public void setLongPrices(Queue<Price> ini, int period){
        long_prices = new MovingAverages(ini, period);
        try {
			long_average = long_prices.calcMA ();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    public void setLongPrices(int period){
        long_prices = new MovingAverages(period);
    }
    
//    //Tracks and updates long and short prices with incoming tag and ticker
//    public String track(double t, String tag){
//        try{
//            if(tag.equals ("long")){
//                long_average = long_prices.addData(t);
//            }else if(tag.equals ("short")){
//                short_average = short_prices.addData(t);
//            }else{
//                return "Incorrect Tag";
//            }
//            
//            //Make decision based on current averages
//            return make_decision();
//        }catch(Exception e){
//            return "Well someone goof'd";
//        }
//    }
    
    //Logic for making decisions
    public Decision make_decision(double currentPrice) throws NotEnoughData, ExitOverThreshold{
    	LOGGER.log(Level.INFO, "Status "+status);
    	if(short_average  < 0 || long_average < 0 ){        
            throw new NotEnoughData();
            } //"Not enough prices"
            
            //Initial. If short prices > long prices buy and set status, vice versa
            if(status==0){
                if(short_average > long_average){
                    //status = 1;
                    return Decision.BUY;
                }
                else{
                    //status = 2;
                    return Decision.SELL;
                }
            }else if(status==9) {
            	throw new ExitOverThreshold();
            }
            else{
            	double change = Math.abs(positionPrice - currentPrice)/100;
            	if(change > threshold) {
            		LOGGER.log(Level.INFO, "Stratgy hit threshold of "+threshold);
            		if(status == 2){
            			status = 9;
                        return Decision.SELL;
                    }else{
                    	status = 9;
                        //status = 2;
                        return Decision.BUY;
                    }
            		//throw new ExitOverThreshold();
            	}
                //Check which average is greater, check status of status and update/return respectively
                if(long_average > short_average){
                    if(status == 2){
                        return Decision.HOLD;
                    }else{
                        //status = 2;
                        return Decision.SELL;
                    }
             
                }
                else{
                    if(status == 1){
                        return Decision.HOLD;
                        
                    }
                    else{
                        //status = 1;
                        return Decision.BUY;
                    }
                }
                
            }
    }
}

