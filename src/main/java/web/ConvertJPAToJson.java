package web;

import java.util.ArrayList;

import persistence.Strategy;
import persistence.Transaction;


/**
 * @author Lelen Abeywardena
 */
public class ConvertJPAToJson {
	
	public static StatusJson createStatusJson(Strategy s, ArrayList<Transaction> trades) {
		
		StrategyTypeJson stj = new StrategyTypeJson();
		stj.setName(s.getStrategyType().getName());
		
		StrategyJson sj = new StrategyJson();
		sj.setID(s.getId());
		sj.setPar1(s.getPar1());
		sj.setPar2(s.getPar2());
		sj.setPar3(s.getPar3());
		sj.setPar4(s.getPar4());
		sj.setTicker(s.getTicker());
		sj.setType(stj);
		if(s.isActive()) {
			sj.setActive("Active");
		}
		else {
			sj.setActive("Inactive");
		}
		
		
		ArrayList<TransactionJson> tjs = new ArrayList<TransactionJson>();
		
		for(Transaction t: trades) {
			tjs.add(new TransactionJson(t.getPosition(), t.getPrice(), t.getQuantity(), t.getTimeplaced()));
		}
		
		
		return new StatusJson(sj, tjs);
		
	}

}
