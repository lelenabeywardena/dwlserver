package web;

import persistence.Strategy;
import persistence.StrategyType;

/**
 * @author Lelen Abeywardena
 */
public class ConvertJsonToJPA {
	
	public static Strategy convertStrategy(StrategyJson json){
		StrategyType st = new StrategyType(json.getType().getName());
		long time = System.currentTimeMillis()/1000L ;
		
		return new Strategy(json.getTicker(), json.getPar1(), json.getPar2(),
				json.getPar3(), json.getPar4(), st, (int)time);
	}
	
	public static StrategyType convertStrategyType(StrategyTypeJson json){
		return new StrategyType(json.getName());
		
	}
	
}
