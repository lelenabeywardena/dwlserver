package web;

import java.util.ArrayList;

import persistence.DBDriver;
import persistence.Strategy;

/**
 * @author Lelen Abeywardena
 */
public class StatusJson {
	
	private StrategyJson strategy;
	private ArrayList<TransactionJson> transactions;
	
	
	public StrategyJson getStrategy() {
		return strategy;
	}


	public void setStrategy(StrategyJson strategy) {
		this.strategy = strategy;
	}


	public ArrayList<TransactionJson> getTransactions() {
		return transactions;
	}


	public void setTransactions(ArrayList<TransactionJson> transactions) {
		this.transactions = transactions;
	}


	public StatusJson(StrategyJson strategy, ArrayList<TransactionJson> transactions) {
		super();
		this.strategy = strategy;
		this.transactions = transactions;
	}

	public static ArrayList<StatusJson> getStrategies() {
		
		ArrayList<StatusJson> strategiesJson = new ArrayList<StatusJson>();
		ArrayList<Strategy> strategies = DBDriver.getInstance().getAllStrategies();
		for(Strategy s: strategies) {
			strategiesJson.add(ConvertJPAToJson.createStatusJson(s, DBDriver.getInstance().getTrades(s)));
		}
		
		return strategiesJson;
	}
	
}
