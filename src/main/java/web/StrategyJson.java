package web;

/**
 * @author Lelen Abeywardena
 *
 */
public class StrategyJson {
	
	private int ID;
	
	private String ticker;
	private int par1;
	private int par2;
	private int par3;
	private int par4;
	private StrategyTypeJson type;
	private String active;
	
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getTicker() {
		return ticker;
	}
	public void setTicker(String ticker) {
		this.ticker = ticker;
	}
	public int getPar1() {
		return par1;
	}
	public void setPar1(int par1) {
		this.par1 = par1;
	}
	public int getPar2() {
		return par2;
	}
	public void setPar2(int par2) {
		this.par2 = par2;
	}
	public int getPar3() {
		return par3;
	}
	public void setPar3(int par3) {
		this.par3 = par3;
	}
	public int getPar4() {
		return par4;
	}
	public void setPar4(int par4) {
		this.par4 = par4;
	}
	public StrategyTypeJson getType() {
		return type;
	}
	public void setType(StrategyTypeJson type) {
		this.type = type;
	}
}
