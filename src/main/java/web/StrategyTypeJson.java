package web;

/**
 * @author Lelen Abeywardena
 *
 */
public class StrategyTypeJson {
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
