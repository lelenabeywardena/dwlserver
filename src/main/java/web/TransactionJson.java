package web;

import java.time.Instant;
import java.util.Date;

/**
 * @author Lelen Abeywardena
 */

public class TransactionJson {
	
	private String position;
	private Double price;
	private int quantity;
	private String timeplaced;
	//convert position to string and timeplaced to string
	public TransactionJson(int position, Double price, int quantity, int timeplaced) {
		super();
		
		if(position ==1) {
			setPosition("Short");
		}else {
			setPosition("Long");
		}
		this.price = price;
		this.quantity = quantity;
		Date date = Date.from( Instant.ofEpochSecond( timeplaced ) );
		setTimeplaced(date.toString());
	}
	
	
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getTimeplaced() {
		return timeplaced;
	}
	public void setTimeplaced(String timeplaced) {
		this.timeplaced = timeplaced;
	}
	

}
