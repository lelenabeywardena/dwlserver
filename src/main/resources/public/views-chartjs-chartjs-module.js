(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-chartjs-chartjs-module"],{

/***/ "./node_modules/rxjs-compat/_esm5/Subject.js":
/*!***************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/Subject.js ***!
  \***************************************************/
/*! exports provided: Subject */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Subject", function() { return rxjs__WEBPACK_IMPORTED_MODULE_0__["Subject"]; });


//# sourceMappingURL=Subject.js.map

/***/ }),

/***/ "./src/app/views/chartjs/chartjs-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/views/chartjs/chartjs-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: ChartJSRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartJSRoutingModule", function() { return ChartJSRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _chartjs_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./chartjs.component */ "./src/app/views/chartjs/chartjs.component.ts");
//Created by Daniel Deng
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    {
        path: '',
        component: _chartjs_component__WEBPACK_IMPORTED_MODULE_4__["ChartJSComponent"],
        data: {
            title: 'Charts'
        }
    }
];
var ChartJSRoutingModule = /** @class */ (function () {
    function ChartJSRoutingModule() {
    }
    ChartJSRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes), _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ChartJSRoutingModule);
    return ChartJSRoutingModule;
}());



/***/ }),

/***/ "./src/app/views/chartjs/chartjs.component.html":
/*!******************************************************!*\
  !*** ./src/app/views/chartjs/chartjs.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Created by Daniel Deng -->\n\n<div>\n  <label for=\"name\">Company Symbol</label>\n  <mwl-text-input-autocomplete-container>\n        <textarea\n          placeholder=\"Type @ to search...\"\n          class=\"form-control\" style=\"height:45px\"\n          rows=\"5\"\n          [(ngModel)]=\"ticker\" (ngModelChange)='changed($event)'\n          mwlTextInputAutocomplete\n          [findChoices]=\"setUpJSON\"\n          [getChoiceLabel]=\"getChoiceLabel\">\n        </textarea>\n  </mwl-text-input-autocomplete-container>\n\n  <br/><br/>\n  <canvas #myCanvas>{{ chart }}</canvas>\n</div>\n"

/***/ }),

/***/ "./src/app/views/chartjs/chartjs.component.ts":
/*!****************************************************!*\
  !*** ./src/app/views/chartjs/chartjs.component.ts ***!
  \****************************************************/
/*! exports provided: ChartJSComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartJSComponent", function() { return ChartJSComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var chart_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! chart.js */ "./node_modules/chart.js/src/chart.js");
/* harmony import */ var chart_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(chart_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_Subject__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/Subject */ "./node_modules/rxjs-compat/_esm5/Subject.js");
/* harmony import */ var rxjs_add_operator_debounceTime__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/add/operator/debounceTime */ "./node_modules/rxjs-compat/_esm5/add/operator/debounceTime.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
//Created by Daniel Deng
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ChartJSComponent = /** @class */ (function () {
    function ChartJSComponent(http) {
        var _this = this;
        this.http = http;
        this.chart = []; //holds chart info
        this.weatherDates = [];
        this.closing = [];
        this.modelChanged = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_4__["Subject"]();
        this.flag = 0;
        this.modelChanged
            .debounceTime(4000) // wait 300ms after the last event before emitting last event
            .subscribe(function (ticker) {
            _this.ticker = ticker;
        });
    }
    ChartJSComponent.prototype.ngOnInit = function () {
        var _this = this;
        Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["interval"])(15000).subscribe(function (x) { return _this.update(); });
    };
    ChartJSComponent.prototype.ngAfterViewInit = function () {
    };
    ChartJSComponent.prototype.changed = function (text) {
        this.weatherDates = [];
        this.closing = [];
        this.create();
        this.modelChanged.next(text);
    };
    //this can be done outside an set as an array
    ChartJSComponent.prototype.setUpJSON = function (searchText) {
        var tickers = [];
        var variable = __webpack_require__(/*! ../dashboard/output_json.json */ "./src/app/views/dashboard/output_json.json");
        for (var i = 0; i < variable.length; i++) {
            tickers.push(variable[i].symbol);
        }
        console.log("wooow" + tickers);
        return tickers.filter(function (item) {
            return item.toLowerCase().includes(searchText.toLowerCase());
        });
    };
    ChartJSComponent.prototype.getChoiceLabel = function (choice) {
        return "@" + choice + " ";
    };
    ChartJSComponent.prototype.create = function () {
        var _this = this;
        var values = [];
        this.tempTicker = this.ticker.substr(1).slice(0, -1);
        var link = "http://incanada1.conygre.com:9080/prices/" + this.tempTicker + "?periods=50";
        this.http.get(link, { responseType: 'text' }).subscribe(function (data) {
            var rows = data.split("\n");
            //console.log(rows);
            for (var i = 1; i < rows.length - 1; i++) {
                var cells = rows[i].split(",");
                var time = cells[0].split(" ");
                var time1 = time[1].split(".");
                _this.weatherDates.push(time1[0]);
                _this.closing.push(cells[4]);
                values.push({
                    "date": time1[0],
                    "close": cells[4],
                    "open": cells[1],
                    "high": cells[2],
                    "low": cells[3],
                    "volume": cells[5],
                    "adjusted": cells[5]
                });
            }
            console.log(_this.weatherDates);
            console.log(_this.closing);
            _this.context = _this.myCanvas.nativeElement.getContext('2d');
            _this.chart = new chart_js__WEBPACK_IMPORTED_MODULE_2__["Chart"](_this.context, {
                type: 'line',
                data: {
                    labels: _this.weatherDates,
                    datasets: [
                        {
                            data: _this.closing,
                            borderColor: '#3cba9f',
                            fill: false
                        }
                    ]
                },
                options: {
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                                display: true
                            }]
                    }
                }
            });
        });
    };
    ChartJSComponent.prototype.update = function () {
        var _this = this;
        console.log("called update");
        if (this.ticker != '') {
            var values = [];
            this.tempTicker = this.ticker.substr(1).slice(0, -1);
            var link = "http://incanada1.conygre.com:9080/prices/" + this.tempTicker + "?periods=50";
            console.log(link);
            this.http.get(link, { responseType: 'text' }).subscribe(function (data) {
                var rows = data.split("\n");
                var i = rows.length - 2;
                var cells = rows[i].split(",");
                var time = cells[0].split(" ");
                var time1 = time[1].split(".");
                _this.weatherDates.push(time1[0]);
                _this.closing.push(cells[4]);
                values.push({
                    "date": time1[0],
                    "close": cells[4],
                    "open": cells[1],
                    "high": cells[2],
                    "low": cells[3],
                    "volume": cells[5],
                    "adjusted": cells[5]
                });
                _this.context = _this.myCanvas.nativeElement.getContext('2d');
                _this.chart = new chart_js__WEBPACK_IMPORTED_MODULE_2__["Chart"](_this.context, {
                    type: 'line',
                    data: {
                        labels: _this.weatherDates,
                        datasets: [
                            {
                                data: _this.closing,
                                borderColor: '#3cba9f',
                                fill: false
                            }
                        ]
                    },
                    options: {
                        legend: {
                            display: false
                        },
                        scales: {
                            yAxes: [{
                                    display: true
                                }]
                        }
                    }
                });
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('myCanvas'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ChartJSComponent.prototype, "myCanvas", void 0);
    ChartJSComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-get-price-new',
            template: __webpack_require__(/*! ./chartjs.component.html */ "./src/app/views/chartjs/chartjs.component.html")
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ChartJSComponent);
    return ChartJSComponent;
}());



/***/ }),

/***/ "./src/app/views/chartjs/chartjs.module.ts":
/*!*************************************************!*\
  !*** ./src/app/views/chartjs/chartjs.module.ts ***!
  \*************************************************/
/*! exports provided: ChartJSModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartJSModule", function() { return ChartJSModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _chartjs_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./chartjs.component */ "./src/app/views/chartjs/chartjs.component.ts");
/* harmony import */ var _chartjs_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./chartjs-routing.module */ "./src/app/views/chartjs/chartjs-routing.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var keyboardevent_key_polyfill__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! keyboardevent-key-polyfill */ "./node_modules/keyboardevent-key-polyfill/index.js");
/* harmony import */ var keyboardevent_key_polyfill__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(keyboardevent_key_polyfill__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var angular_text_input_autocomplete__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angular-text-input-autocomplete */ "./node_modules/angular-text-input-autocomplete/fesm5/angular-text-input-autocomplete.js");
//Created by Daniel Deng
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







Object(keyboardevent_key_polyfill__WEBPACK_IMPORTED_MODULE_5__["polyfill"])();
var ChartJSModule = /** @class */ (function () {
    function ChartJSModule() {
    }
    ChartJSModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _chartjs_routing_module__WEBPACK_IMPORTED_MODULE_3__["ChartJSRoutingModule"],
                ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_1__["ChartsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                angular_text_input_autocomplete__WEBPACK_IMPORTED_MODULE_6__["TextInputAutocompleteModule"],
            ],
            declarations: [_chartjs_component__WEBPACK_IMPORTED_MODULE_2__["ChartJSComponent"]]
        })
    ], ChartJSModule);
    return ChartJSModule;
}());



/***/ })

}]);
//# sourceMappingURL=views-chartjs-chartjs-module.js.map