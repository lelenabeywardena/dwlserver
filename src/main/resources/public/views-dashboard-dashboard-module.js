(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-dashboard-dashboard-module"],{

/***/ "./node_modules/ngx-bootstrap/buttons/button-checkbox.directive.js":
/*!*************************************************************************!*\
  !*** ./node_modules/ngx-bootstrap/buttons/button-checkbox.directive.js ***!
  \*************************************************************************/
/*! exports provided: CHECKBOX_CONTROL_VALUE_ACCESSOR, ButtonCheckboxDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CHECKBOX_CONTROL_VALUE_ACCESSOR", function() { return CHECKBOX_CONTROL_VALUE_ACCESSOR; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ButtonCheckboxDirective", function() { return ButtonCheckboxDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");


// TODO: config: activeClass - Class to apply to the checked buttons
var CHECKBOX_CONTROL_VALUE_ACCESSOR = {
    provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NG_VALUE_ACCESSOR"],
    useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(function () { return ButtonCheckboxDirective; }),
    multi: true
};
/**
 * Add checkbox functionality to any element
 */
var ButtonCheckboxDirective = /** @class */ (function () {
    function ButtonCheckboxDirective() {
        /** Truthy value, will be set to ngModel */
        this.btnCheckboxTrue = true;
        /** Falsy value, will be set to ngModel */
        this.btnCheckboxFalse = false;
        this.state = false;
        this.onChange = Function.prototype;
        this.onTouched = Function.prototype;
    }
    // view -> model
    ButtonCheckboxDirective.prototype.onClick = 
    // view -> model
    function () {
        if (this.isDisabled) {
            return;
        }
        this.toggle(!this.state);
        this.onChange(this.value);
    };
    ButtonCheckboxDirective.prototype.ngOnInit = function () {
        this.toggle(this.trueValue === this.value);
    };
    Object.defineProperty(ButtonCheckboxDirective.prototype, "trueValue", {
        get: function () {
            return typeof this.btnCheckboxTrue !== 'undefined'
                ? this.btnCheckboxTrue
                : true;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ButtonCheckboxDirective.prototype, "falseValue", {
        get: function () {
            return typeof this.btnCheckboxFalse !== 'undefined'
                ? this.btnCheckboxFalse
                : false;
        },
        enumerable: true,
        configurable: true
    });
    ButtonCheckboxDirective.prototype.toggle = function (state) {
        this.state = state;
        this.value = this.state ? this.trueValue : this.falseValue;
    };
    // ControlValueAccessor
    // model -> view
    // ControlValueAccessor
    // model -> view
    ButtonCheckboxDirective.prototype.writeValue = 
    // ControlValueAccessor
    // model -> view
    function (value) {
        this.state = this.trueValue === value;
        this.value = value ? this.trueValue : this.falseValue;
    };
    ButtonCheckboxDirective.prototype.setDisabledState = function (isDisabled) {
        this.isDisabled = isDisabled;
    };
    ButtonCheckboxDirective.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    ButtonCheckboxDirective.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    ButtonCheckboxDirective.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                    selector: '[btnCheckbox]',
                    providers: [CHECKBOX_CONTROL_VALUE_ACCESSOR]
                },] },
    ];
    /** @nocollapse */
    ButtonCheckboxDirective.propDecorators = {
        "btnCheckboxTrue": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        "btnCheckboxFalse": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        "state": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.active',] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['attr.aria-pressed',] },],
        "onClick": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"], args: ['click',] },],
    };
    return ButtonCheckboxDirective;
}());

//# sourceMappingURL=button-checkbox.directive.js.map

/***/ }),

/***/ "./node_modules/ngx-bootstrap/buttons/button-radio-group.directive.js":
/*!****************************************************************************!*\
  !*** ./node_modules/ngx-bootstrap/buttons/button-radio-group.directive.js ***!
  \****************************************************************************/
/*! exports provided: RADIO_CONTROL_VALUE_ACCESSOR, ButtonRadioGroupDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RADIO_CONTROL_VALUE_ACCESSOR", function() { return RADIO_CONTROL_VALUE_ACCESSOR; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ButtonRadioGroupDirective", function() { return ButtonRadioGroupDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");


var RADIO_CONTROL_VALUE_ACCESSOR = {
    provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NG_VALUE_ACCESSOR"],
    useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(function () { return ButtonRadioGroupDirective; }),
    multi: true
};
/**
 * A group of radio buttons.
 * A value of a selected button is bound to a variable specified via ngModel.
 */
var ButtonRadioGroupDirective = /** @class */ (function () {
    function ButtonRadioGroupDirective(el, cdr) {
        this.el = el;
        this.cdr = cdr;
        this.onChange = Function.prototype;
        this.onTouched = Function.prototype;
    }
    Object.defineProperty(ButtonRadioGroupDirective.prototype, "value", {
        get: function () {
            return this._value;
        },
        set: function (value) {
            this._value = value;
        },
        enumerable: true,
        configurable: true
    });
    ButtonRadioGroupDirective.prototype.writeValue = function (value) {
        this._value = value;
        this.cdr.markForCheck();
    };
    ButtonRadioGroupDirective.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    ButtonRadioGroupDirective.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    ButtonRadioGroupDirective.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                    selector: '[btnRadioGroup]',
                    providers: [RADIO_CONTROL_VALUE_ACCESSOR]
                },] },
    ];
    /** @nocollapse */
    ButtonRadioGroupDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"], },
    ]; };
    return ButtonRadioGroupDirective;
}());

//# sourceMappingURL=button-radio-group.directive.js.map

/***/ }),

/***/ "./node_modules/ngx-bootstrap/buttons/button-radio.directive.js":
/*!**********************************************************************!*\
  !*** ./node_modules/ngx-bootstrap/buttons/button-radio.directive.js ***!
  \**********************************************************************/
/*! exports provided: RADIO_CONTROL_VALUE_ACCESSOR, ButtonRadioDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RADIO_CONTROL_VALUE_ACCESSOR", function() { return RADIO_CONTROL_VALUE_ACCESSOR; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ButtonRadioDirective", function() { return ButtonRadioDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _button_radio_group_directive__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./button-radio-group.directive */ "./node_modules/ngx-bootstrap/buttons/button-radio-group.directive.js");



var RADIO_CONTROL_VALUE_ACCESSOR = {
    provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NG_VALUE_ACCESSOR"],
    useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(function () { return ButtonRadioDirective; }),
    multi: true
};
/**
 * Create radio buttons or groups of buttons.
 * A value of a selected button is bound to a variable specified via ngModel.
 */
var ButtonRadioDirective = /** @class */ (function () {
    function ButtonRadioDirective(el, cdr, group, renderer) {
        this.el = el;
        this.cdr = cdr;
        this.group = group;
        this.renderer = renderer;
        this.onChange = Function.prototype;
        this.onTouched = Function.prototype;
    }
    Object.defineProperty(ButtonRadioDirective.prototype, "value", {
        get: /** Current value of radio component or group */
        function () {
            return this.group ? this.group.value : this._value;
        },
        set: function (value) {
            if (this.group) {
                this.group.value = value;
                return;
            }
            this._value = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ButtonRadioDirective.prototype, "disabled", {
        get: /** If `true` — radio button is disabled */
        function () {
            return this._disabled;
        },
        set: function (disabled) {
            this._disabled = disabled;
            this.setDisabledState(disabled);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ButtonRadioDirective.prototype, "isActive", {
        get: function () {
            return this.btnRadio === this.value;
        },
        enumerable: true,
        configurable: true
    });
    ButtonRadioDirective.prototype.onClick = function () {
        if (this.el.nativeElement.attributes.disabled || !this.uncheckable && this.btnRadio === this.value) {
            return;
        }
        this.value = this.uncheckable && this.btnRadio === this.value ? undefined : this.btnRadio;
        this._onChange(this.value);
    };
    ButtonRadioDirective.prototype.ngOnInit = function () {
        this.uncheckable = typeof this.uncheckable !== 'undefined';
    };
    ButtonRadioDirective.prototype.onBlur = function () {
        this.onTouched();
    };
    ButtonRadioDirective.prototype._onChange = function (value) {
        if (this.group) {
            this.group.onTouched();
            this.group.onChange(value);
            return;
        }
        this.onTouched();
        this.onChange(value);
    };
    // ControlValueAccessor
    // model -> view
    // ControlValueAccessor
    // model -> view
    ButtonRadioDirective.prototype.writeValue = 
    // ControlValueAccessor
    // model -> view
    function (value) {
        this.value = value;
        this.cdr.markForCheck();
    };
    ButtonRadioDirective.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    ButtonRadioDirective.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    ButtonRadioDirective.prototype.setDisabledState = function (disabled) {
        if (disabled) {
            this.renderer.setAttribute(this.el.nativeElement, 'disabled', 'disabled');
            return;
        }
        this.renderer.removeAttribute(this.el.nativeElement, 'disabled');
    };
    ButtonRadioDirective.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                    selector: '[btnRadio]',
                    providers: [RADIO_CONTROL_VALUE_ACCESSOR]
                },] },
    ];
    /** @nocollapse */
    ButtonRadioDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"], },
        { type: _button_radio_group_directive__WEBPACK_IMPORTED_MODULE_2__["ButtonRadioGroupDirective"], decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"] },] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"], },
    ]; };
    ButtonRadioDirective.propDecorators = {
        "btnRadio": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        "uncheckable": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        "value": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        "disabled": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        "isActive": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.active',] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['attr.aria-pressed',] },],
        "onClick": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"], args: ['click',] },],
    };
    return ButtonRadioDirective;
}());

//# sourceMappingURL=button-radio.directive.js.map

/***/ }),

/***/ "./node_modules/ngx-bootstrap/buttons/buttons.module.js":
/*!**************************************************************!*\
  !*** ./node_modules/ngx-bootstrap/buttons/buttons.module.js ***!
  \**************************************************************/
/*! exports provided: ButtonsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ButtonsModule", function() { return ButtonsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _button_checkbox_directive__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./button-checkbox.directive */ "./node_modules/ngx-bootstrap/buttons/button-checkbox.directive.js");
/* harmony import */ var _button_radio_directive__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./button-radio.directive */ "./node_modules/ngx-bootstrap/buttons/button-radio.directive.js");
/* harmony import */ var _button_radio_group_directive__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./button-radio-group.directive */ "./node_modules/ngx-bootstrap/buttons/button-radio-group.directive.js");




var ButtonsModule = /** @class */ (function () {
    function ButtonsModule() {
    }
    ButtonsModule.forRoot = function () {
        return { ngModule: ButtonsModule, providers: [] };
    };
    ButtonsModule.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                    declarations: [_button_checkbox_directive__WEBPACK_IMPORTED_MODULE_1__["ButtonCheckboxDirective"], _button_radio_directive__WEBPACK_IMPORTED_MODULE_2__["ButtonRadioDirective"], _button_radio_group_directive__WEBPACK_IMPORTED_MODULE_3__["ButtonRadioGroupDirective"]],
                    exports: [_button_checkbox_directive__WEBPACK_IMPORTED_MODULE_1__["ButtonCheckboxDirective"], _button_radio_directive__WEBPACK_IMPORTED_MODULE_2__["ButtonRadioDirective"], _button_radio_group_directive__WEBPACK_IMPORTED_MODULE_3__["ButtonRadioGroupDirective"]]
                },] },
    ];
    return ButtonsModule;
}());

//# sourceMappingURL=buttons.module.js.map

/***/ }),

/***/ "./node_modules/ngx-bootstrap/buttons/index.js":
/*!*****************************************************!*\
  !*** ./node_modules/ngx-bootstrap/buttons/index.js ***!
  \*****************************************************/
/*! exports provided: ButtonCheckboxDirective, ButtonRadioGroupDirective, ButtonRadioDirective, ButtonsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _button_checkbox_directive__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./button-checkbox.directive */ "./node_modules/ngx-bootstrap/buttons/button-checkbox.directive.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ButtonCheckboxDirective", function() { return _button_checkbox_directive__WEBPACK_IMPORTED_MODULE_0__["ButtonCheckboxDirective"]; });

/* harmony import */ var _button_radio_group_directive__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./button-radio-group.directive */ "./node_modules/ngx-bootstrap/buttons/button-radio-group.directive.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ButtonRadioGroupDirective", function() { return _button_radio_group_directive__WEBPACK_IMPORTED_MODULE_1__["ButtonRadioGroupDirective"]; });

/* harmony import */ var _button_radio_directive__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./button-radio.directive */ "./node_modules/ngx-bootstrap/buttons/button-radio.directive.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ButtonRadioDirective", function() { return _button_radio_directive__WEBPACK_IMPORTED_MODULE_2__["ButtonRadioDirective"]; });

/* harmony import */ var _buttons_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./buttons.module */ "./node_modules/ngx-bootstrap/buttons/buttons.module.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ButtonsModule", function() { return _buttons_module__WEBPACK_IMPORTED_MODULE_3__["ButtonsModule"]; });





//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/views/dashboard/dashboard-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/views/dashboard/dashboard-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: DashboardRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardRoutingModule", function() { return DashboardRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _strategydetails_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./strategydetails.component */ "./src/app/views/dashboard/strategydetails.component.ts");
/* harmony import */ var _dashboard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dashboard.component */ "./src/app/views/dashboard/dashboard.component.ts");
//Created by Daniel Deng
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    {
        path: '',
        component: _dashboard_component__WEBPACK_IMPORTED_MODULE_3__["DashboardComponent"],
        data: {
            title: 'Dashboard'
        }
    }
];
var DashboardRoutingModule = /** @class */ (function () {
    function DashboardRoutingModule() {
    }
    DashboardRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_strategydetails_component__WEBPACK_IMPORTED_MODULE_2__["StrategyDetailsComponent"]],
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
        })
    ], DashboardRoutingModule);
    return DashboardRoutingModule;
}());



/***/ }),

/***/ "./src/app/views/dashboard/dashboard.component.html":
/*!**********************************************************!*\
  !*** ./src/app/views/dashboard/dashboard.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Created by Daniel Deng -->\n\n<div class=\"animated fadeIn\">\n  <div class=\"row\">\n    <div class=\"col-sm-6 col-lg-3\">\n      <div class=\"card text-white bg-primary\">\n        <div class=\"card-body pb-0\">\n          <div class=\"btn-group float-right\" dropdown>\n            <button type=\"button\" class=\"btn btn-transparent dropdown-toggle p-0\" dropdownToggle>\n              <i class=\"icon-settings\"></i>\n            </button>\n            <div class=\"dropdown-menu dropdown-menu-right\" *dropdownMenu>\n              <a class=\"dropdown-item\" href=\"#\">Action</a>\n              <a class=\"dropdown-item\" href=\"#\">Another action</a>\n              <a class=\"dropdown-item\" href=\"#\">Something else here</a>\n              <a class=\"dropdown-item\" href=\"#\">Something else here</a>\n            </div>\n          </div>\n          <div class=\"text-value\">$100.51</div>\n          <div>Apple</div>\n        </div>\n        <div class=\"chart-wrapper mt-3 mx-3\" style=\"height:70px;\">\n          <canvas baseChart class=\"chart\"\n          [datasets]=\"lineChart1Data\"\n          [labels]=\"lineChart1Labels\"\n          [options]=\"lineChart1Options\"\n          [colors]=\"lineChart1Colours\"\n          [legend]=\"lineChart1Legend\"\n          [chartType]=\"lineChart1Type\"></canvas>\n        </div>\n      </div>\n    </div><!--/.col-->\n    <div class=\"col-sm-6 col-lg-3\">\n      <div class=\"card text-white bg-info\">\n        <div class=\"card-body pb-0\">\n          <button type=\"button\" class=\"btn btn-transparent p-0 float-right\">\n            <i class=\"icon-location-pin\"></i>\n          </button>\n          <div class=\"text-value\">$507.90</div>\n          <div>Facebook</div>\n        </div>\n        <div class=\"chart-wrapper mt-3 mx-3\" style=\"height:70px;\">\n          <canvas baseChart class=\"chart\"\n          [datasets]=\"lineChart2Data\"\n          [labels]=\"lineChart2Labels\"\n          [options]=\"lineChart2Options\"\n          [colors]=\"lineChart2Colours\"\n          [legend]=\"lineChart2Legend\"\n          [chartType]=\"lineChart2Type\"></canvas>\n        </div>\n      </div>\n    </div><!--/.col-->\n    <div class=\"col-sm-6 col-lg-3\">\n      <div class=\"card text-white bg-warning\">\n        <div class=\"card-body pb-0\">\n          <div class=\"btn-group float-right\" dropdown>\n            <button type=\"button\" class=\"btn btn-transparent dropdown-toggle p-0\" dropdownToggle>\n              <i class=\"icon-settings\"></i>\n            </button>\n            <div class=\"dropdown-menu dropdown-menu-right\" *dropdownMenu>\n              <a class=\"dropdown-item\" href=\"#\">Action</a>\n              <a class=\"dropdown-item\" href=\"#\">Another action</a>\n              <a class=\"dropdown-item\" href=\"#\">Something else here</a>\n            </div>\n          </div>\n          <div class=\"text-value\">$180.67</div>\n          <div>Shopify</div>\n        </div>\n        <div class=\"chart-wrapper mt-3\" style=\"height:70px;\">\n          <canvas baseChart class=\"chart\"\n          [datasets]=\"lineChart3Data\"\n          [labels]=\"lineChart3Labels\"\n          [options]=\"lineChart3Options\"\n          [colors]=\"lineChart3Colours\"\n          [legend]=\"lineChart3Legend\"\n          [chartType]=\"lineChart3Type\"></canvas>\n        </div>\n      </div>\n    </div><!--/.col-->\n    <div class=\"col-sm-6 col-lg-3\">\n      <div class=\"card text-white bg-danger\">\n        <div class=\"card-body pb-0\">\n          <div class=\"btn-group float-right\" dropdown>\n            <button type=\"button\" class=\"btn btn-transparent dropdown-toggle p-0\" dropdownToggle>\n              <i class=\"icon-settings\"></i>\n            </button>\n            <div class=\"dropdown-menu dropdown-menu-right\" *dropdownMenu>\n              <a class=\"dropdown-item\" href=\"#\">Action</a>\n              <a class=\"dropdown-item\" href=\"#\">Another action</a>\n              <a class=\"dropdown-item\" href=\"#\">Something else here</a>\n            </div>\n          </div>\n          <div class=\"text-value\">$64.37</div>\n          <div>Citi</div>\n        </div>\n        <div class=\"chart-wrapper mt-3 mx-3\" style=\"height:70px;\" id=\"tradingForm\"  >\n          <canvas baseChart class=\"chart\"\n          [datasets]=\"barChart1Data\"\n          [labels]=\"barChart1Labels\"\n          [options]=\"barChart1Options\"\n          [colors]=\"barChart1Colours\"\n          [legend]=\"barChart1Legend\"\n          [chartType]=\"barChart1Type\"></canvas>\n        </div>\n      </div>\n    </div><!--/.col-->\n  </div><!--/.row-->\n\n\n\n\n  <!-- Trading Form -->\n  <div class=\"row\">\n    <div class=\"col-sm-12\">\n      <div class=\"card\">\n        <div class=\"card-header\">\n          <strong style=\"font-size: 22px\">Create Strategy</strong>\n        </div>\n        <div class=\"card-body\">\n\n\n          <div class=\"form-group row\">\n            <label class=\"col-md-3 col-form-label\" for=\"select1\">Strategy</label>\n            <div class=\"col-md-12\">\n              <select [(ngModel)]=\"name\" id=\"select1\" name=\"select1\" class=\"form-control\">\n                <option value=\"TWOMA\">Two Moving Averages</option>\n                <option value=\"1\">Strategy 2</option>\n                <option value=\"2\">Strategy 3</option>\n              </select>\n            </div>\n          </div>\n\n          <div class=\"row\">\n            <div class=\"col-sm-12\">\n              <div class=\"form-group\">\n                <label for=\"name\">Company Symbol</label>\n                <mwl-text-input-autocomplete-container>\n                      <textarea\n                        placeholder=\"Type @ to search...\"\n                        class=\"form-control\" style=\"height:45px\"\n                        rows=\"5\"\n                        [(ngModel)]=\"ticker\"\n                        mwlTextInputAutocomplete\n                        [findChoices]=\"setUpJSON\"\n                        [getChoiceLabel]=\"getChoiceLabel\">\n                      </textarea>\n                </mwl-text-input-autocomplete-container><br/>\n              </div>\n            </div>\n          </div>\n\n\n\n\n\n\n\n\n\n\n\n\n          <div class=\"row\">\n              <div class=\"col-sm-6\">\n                <div class=\"form-group\">\n                  <label for=\"name\">SMA Period</label>\n                  <input [(ngModel)]=\"par1\" type=\"number\" class=\"form-control\" id=\"name\" placeholder=\"Enter the Short Moving Average period\">\n                </div>\n              </div>\n\n              <div class=\"col-sm-6\">\n                <div class=\"form-group\">\n                  <label for=\"name\">LMA Period</label>\n                  <input [(ngModel)]=\"par2\" type=\"number\" class=\"form-control\" id=\"name\" placeholder=\"Enter the Long Moving Average period\">\n                </div>\n              </div>\n          </div><!--/.row-->\n\n\n          <div class=\"row\">\n\n            <div class=\"col-sm-6\">\n              <div class=\"form-group\">\n                <label class=\"col-form-label\" for=\"appendedInput\">Threshold</label>\n                <div class=\"controls\">\n                  <div class=\"input-group\">\n                    <input [(ngModel)]=\"par3\" id=\"appendedInput\" class=\"form-control\" size=\"16\" type=\"number\">\n                    <div class=\"input-group-append\">\n                      <span class=\"input-group-text\">%</span>\n                    </div>\n                  </div>\n                  <span class=\"help-block\">Enter your exit threshold for loss and gains</span>\n                </div>\n              </div>\n            </div>\n\n            <div class=\"col-sm-6\">\n              <div class=\"form-group\">\n                <label class=\"col-form-label\" for=\"prependedInput\">Size</label>\n                <div class=\"controls\">\n                  <div class=\"input-prepend input-group\">\n                    <div class=\"input-group-prepend\">\n                      <span class=\"input-group-text\">$</span>\n                    </div>\n                    <input [(ngModel)]=\"par4\" id=\"prependedInput\" class=\"form-control\" size=\"16\" type=\"text\">\n                  </div>\n                  <p class=\"help-block\">Size of position in dollars allocated to this strategy</p>\n                </div>\n              </div>\n            </div>\n          </div><!--/.row-->\n\n        </div>\n\n        <div class=\"form-actions\">\n          <button class=\"btn btn-secondary\" id=\"clear\" type=\"button\" (click)=\"clearForm();\">Clear</button>\n          <button type=\"submit\" (click)=\"createTrade(); clearForm()\" class=\"btn btn-primary\" id=\"submit\">Submit</button>\n        </div>\n      </div>\n    </div><!--/.col-->\n  </div><!--/.row-->\n<br/>\n\n\n  <div class=\"row\">\n    <div class=\"col-md-12\">\n      <div class=\"card\">\n        <div class=\"card-header\">\n          <strong style=\"font-size: 22px\">Strategy List</strong>\n        </div>\n        <div class=\"card-body\">\n          <table class=\"table table-responsive-sm table-hover table-outline mb-0\">\n\n\n            <thead class=\"thead-light\">\n              <tr>\n                <th class=\"text-center\">Identifier</th>\n                <th class=\"text-center\">Strategy Type</th>\n                <th class=\"text-center\">Ticker</th>\n                <th class=\"text-center\">Threshold</th>\n                <th class=\"text-center\">Position ($)</th>\n                <th class=\"text-center\">Status</th>\n              </tr>\n            </thead>\n\n\n            <tbody>\n              <tr *ngFor=\"let strategy of strategies\" (click)=\"getDetails(strategy.transactions, strategy.strategy.id)\">\n\n                <td class=\"text-center\">\n                  <div>{{strategy.strategy.id}}</div>\n                </td>\n\n                <!-- strategy type header -->\n                <td class=\"text-center\">\n                  <div>{{strategy.strategy.type.name}}</div>\n                </td>\n\n                <!-- ticker headers -->\n                <td class=\"text-center\">\n                  <div>{{strategy.strategy.ticker}}</div>\n                </td>\n\n                <!-- threshold header -->\n                <td class=\"text-center\">\n                  <div>{{strategy.strategy.par3}}</div>\n                </td>\n\n                <!-- position header -->\n                <td class=\"text-center\">\n                  <div>{{strategy.strategy.par4}}</div>\n                </td>\n\n                <!-- status header -->\n                <td class=\"text-center\">\n                  <div>{{strategy.strategy.active}}</div>\n                  <a href=\"#tradingForm\"><button class=\"btn btn-secondary\" type=\"button\" (click)=\"adjust(strategy.strategy);\">Adjust</button></a>\n                </td>\n              </tr>\n\n\n            </tbody>\n          </table>\n          <br/><br/>\n          <div>\n            <label for=\"name\">Stop or Stop Strategies</label>\n            <input [(ngModel)]=\"decision\" type=\"number\" class=\"form-control\" id=\"name\" placeholder=\"Enter the Identifier of the strategy you wish to start or stop\">\n          </div>\n          <br/>\n          <div>\n            <button class=\"btn btn-secondary\" id=\"stop\" type=\"button\" (click)=\"stopTrade();\">Stop</button>\n          </div>\n\n          <div>\n            <button class=\"btn btn-secondary\" id=\"start\" type=\"button\" (click)=\"startTrade();\">Start</button>\n          </div>\n\n        </div>\n      </div>\n\n\n\n\n\n      <div *ngIf=\"show\">\n        <div class=\"card-header\">\n          <strong style=\"font-size: 22px\">Historical Trades for Strategy: {{strategyId}}</strong>\n        </div>\n\n                <table class=\"table table-responsive-sm table-hover table-outline mb-0\">\n\n\n                  <thead class=\"thead-light\">\n                    <tr>\n                      <th class=\"text-center\">Position</th>\n                      <th class=\"text-center\">Price</th>\n                      <th class=\"text-center\">Quantity</th>\n                      <th class=\"text-center\">Time Placed</th>\n                    </tr>\n                  </thead>\n\n\n\n                  <tbody>\n                    <tr *ngFor=\"let transaction of transactions\">\n\n                      <td class=\"text-center\">\n                        <div>{{transaction.position}}</div>\n                      </td>\n\n                      <!-- strategy type header -->\n                      <td class=\"text-center\">\n                        <div>{{transaction.price}}</div>\n                      </td>\n\n                      <!-- ticker headers -->\n                      <td class=\"text-center\">\n                        <div>{{transaction.quantity}}</div>\n                      </td>\n\n                      <!-- threshold header -->\n                      <td class=\"text-center\">\n                        <div>{{transaction.timeplaced}}</div>\n                      </td>\n\n\n                    </tr>\n\n\n                  </tbody>\n                </table>\n      </div>\n\n\n\n\n\n\n\n      <div>\n        <div class=\"card-header\">\n          <strong style=\"font-size: 22px\">Performance</strong>\n        </div>\n\n                <table class=\"table table-responsive-sm table-hover table-outline mb-0\">\n\n\n                  <thead class=\"thead-light\">\n                    <tr>\n                      <th class=\"text-center\">Strategy Id</th>\n                      <th class=\"text-center\">Gains/Losses</th>\n                    </tr>\n                  </thead>\n\n\n\n                  <tbody>\n                    <tr *ngFor=\"let performance of performances\" [ngStyle]=\"{'background-color': getColor(performance.gain), 'color': 'white'}\">\n\n                      <td class=\"text-center\">\n                        <div>{{performance.id}}</div>\n                      </td>\n\n                      <!-- strategy type header -->\n                      <td class=\"text-center\">\n                        <div>{{performance.gain}}</div>\n                      </td>\n\n                    </tr>\n\n                  </tbody>\n                </table>\n\n                <br/>\n                <div>\n                  <button class=\"btn btn-secondary\" id=\"clear\" type=\"button\" (click)=\"refreshPerformance();\">Refresh</button>\n                </div>\n      </div>\n\n\n    </div><!--/.col-->\n  </div><!--/.row-->\n</div>\n"

/***/ }),

/***/ "./src/app/views/dashboard/dashboard.component.ts":
/*!********************************************************!*\
  !*** ./src/app/views/dashboard/dashboard.component.ts ***!
  \********************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _trade__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./trade */ "./src/app/views/dashboard/trade.ts");
/* harmony import */ var _dashboard_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dashboard.service */ "./src/app/views/dashboard/dashboard.service.ts");
/* harmony import */ var rxjs_add_operator_debounceTime__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/debounceTime */ "./node_modules/rxjs-compat/_esm5/add/operator/debounceTime.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var _strategy__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./strategy */ "./src/app/views/dashboard/strategy.ts");
//Created by Daniel Deng
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(tradeService) {
        this.tradeService = tradeService;
        this.trade = new _trade__WEBPACK_IMPORTED_MODULE_1__["Trade"]();
        this.ticker = '';
        this.show = false;
        this.status = [];
    }
    //this can be done outside an set as an array
    DashboardComponent.prototype.setUpJSON = function (searchText) {
        var tickers = [];
        var variable = __webpack_require__(/*! ./output_json.json */ "./src/app/views/dashboard/output_json.json");
        for (var i = 0; i < variable.length; i++) {
            tickers.push(variable[i].symbol);
        }
        console.log("wooow" + tickers);
        return tickers.filter(function (item) {
            return item.toLowerCase().includes(searchText.toLowerCase());
        });
    };
    DashboardComponent.prototype.getChoiceLabel = function (choice) {
        return "@" + choice + " ";
    };
    // findChoices(searchText: string) {
    //
    //   let tickerArray =
    //
    //   let tickerArray = this.setUpJSON(function(x) => {
    //     console.log(tickerArray);
    //     return x.filter(item => {
    //       item.toLowerCase().includes(searchText.toLowerCase())
    //     });
    //   });
    // }
    DashboardComponent.prototype.random = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    };
    DashboardComponent.prototype.ngOnInit = function () {
        this.getAllTrades();
        this.refreshPerformance();
    };
    DashboardComponent.prototype.createTrade = function () {
        var _this = this;
        this.trade.par1 = this.par1;
        this.trade.par2 = this.par2;
        this.trade.par3 = this.par3;
        this.trade.par4 = this.par4;
        this.trade.type = new _strategy__WEBPACK_IMPORTED_MODULE_5__["Strategy"]();
        this.trade.type.name = this.name;
        this.trade.ticker = this.ticker;
        console.log(this.trade);
        this.tradeService.createTrade(this.trade)
            .subscribe(function (result) {
            console.log("wow");
            _this.getAllTrades();
        });
    };
    DashboardComponent.prototype.startTrade = function () {
        var _this = this;
        this.tradeService.startTrade(this.decision)
            .subscribe(function (result) {
            console.log("starteddddd");
            _this.getAllTrades();
        });
    };
    DashboardComponent.prototype.stopTrade = function () {
        var _this = this;
        this.tradeService.stopTrade(this.decision)
            .subscribe(function (result) {
            console.log("stopped");
            _this.getAllTrades();
        });
    };
    DashboardComponent.prototype.getAllTrades = function () {
        var _this = this;
        this.tradeService.getAllTrades()
            .subscribe(function (result) {
            console.log(result);
            _this.strategies = result;
        });
    };
    DashboardComponent.prototype.getAllTransactions = function () {
        console.log(this.strategies);
    };
    DashboardComponent.prototype.refreshPerformance = function () {
        var _this = this;
        this.tradeService.refreshPerformance()
            .subscribe(function (result) {
            console.log(result);
            _this.performances = result;
        });
    };
    DashboardComponent.prototype.getColor = function (performance) {
        if (performance.charAt(0) == '-') {
            return '#FF4C4C';
        }
        else {
            return '#7FB146';
        }
    };
    DashboardComponent.prototype.adjust = function (strategy) {
        console.log(strategy.name);
        this.name = strategy.name;
        this.ticker = strategy.ticker;
        this.par1 = strategy.par1;
        this.par2 = strategy.par2;
        this.par3 = strategy.par3;
        this.par4 = strategy.par4;
        console.log(strategy);
    };
    DashboardComponent.prototype.getDetails = function (transactions, id) {
        console.log(transactions);
        this.show = true;
        this.strategyId = id;
        this.transactions = transactions;
    };
    // populateStatus(result) {
    //   for (let i=0; i<result.length; i++) {
    //     this.status.push(result[i].strategy.active);
    //   }
    // }
    //
    // isPaused = false;
    // text = "Play";
    //
    // click() {
    //   console.log("clicked");
    //   this.isPaused = !this.isPaused;
    //   this.text = this.isPaused ? "Pause" : "Play";
    // }
    DashboardComponent.prototype.clearForm = function () {
        console.log("clear form");
        this.ticker = null;
        this.par1 = null;
        this.par2 = null;
        this.par3 = null;
        this.par4 = null;
        this.name = null;
    };
    DashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/views/dashboard/dashboard.component.html"),
        }),
        __metadata("design:paramtypes", [_dashboard_service__WEBPACK_IMPORTED_MODULE_2__["TradeService"]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/views/dashboard/dashboard.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/views/dashboard/dashboard.module.ts ***!
  \*****************************************************/
/*! exports provided: DashboardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardModule", function() { return DashboardModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-bootstrap/dropdown */ "./node_modules/ngx-bootstrap/dropdown/index.js");
/* harmony import */ var ngx_bootstrap_buttons__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-bootstrap/buttons */ "./node_modules/ngx-bootstrap/buttons/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _dashboard_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./dashboard.component */ "./src/app/views/dashboard/dashboard.component.ts");
/* harmony import */ var _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./dashboard-routing.module */ "./src/app/views/dashboard/dashboard-routing.module.ts");
/* harmony import */ var keyboardevent_key_polyfill__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! keyboardevent-key-polyfill */ "./node_modules/keyboardevent-key-polyfill/index.js");
/* harmony import */ var keyboardevent_key_polyfill__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(keyboardevent_key_polyfill__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var angular_text_input_autocomplete__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! angular-text-input-autocomplete */ "./node_modules/angular-text-input-autocomplete/fesm5/angular-text-input-autocomplete.js");
//Created by Daniel Deng
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










Object(keyboardevent_key_polyfill__WEBPACK_IMPORTED_MODULE_8__["polyfill"])();
var DashboardModule = /** @class */ (function () {
    function DashboardModule() {
    }
    DashboardModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_7__["DashboardRoutingModule"],
                ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_1__["ChartsModule"],
                ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_2__["BsDropdownModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                angular_text_input_autocomplete__WEBPACK_IMPORTED_MODULE_9__["TextInputAutocompleteModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_5__["CommonModule"],
                ngx_bootstrap_buttons__WEBPACK_IMPORTED_MODULE_3__["ButtonsModule"].forRoot()
            ],
            declarations: [_dashboard_component__WEBPACK_IMPORTED_MODULE_6__["DashboardComponent"]]
        })
    ], DashboardModule);
    return DashboardModule;
}());



/***/ }),

/***/ "./src/app/views/dashboard/strategy.ts":
/*!*********************************************!*\
  !*** ./src/app/views/dashboard/strategy.ts ***!
  \*********************************************/
/*! exports provided: Strategy */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Strategy", function() { return Strategy; });
var Strategy = /** @class */ (function () {
    function Strategy() {
    }
    return Strategy;
}());



/***/ }),

/***/ "./src/app/views/dashboard/strategydetails.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/views/dashboard/strategydetails.component.ts ***!
  \**************************************************************/
/*! exports provided: StrategyDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StrategyDetailsComponent", function() { return StrategyDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_add_operator_debounceTime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/add/operator/debounceTime */ "./node_modules/rxjs-compat/_esm5/add/operator/debounceTime.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var StrategyDetailsComponent = /** @class */ (function () {
    function StrategyDetailsComponent() {
    }
    StrategyDetailsComponent.prototype.ngOnInit = function () {
    };
    StrategyDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            // tslint:disable-next-line
            selector: 'strategydetails',
            template: __webpack_require__(/*! ./strategydetails.html */ "./src/app/views/dashboard/strategydetails.html"),
        }),
        __metadata("design:paramtypes", [])
    ], StrategyDetailsComponent);
    return StrategyDetailsComponent;
}());



/***/ }),

/***/ "./src/app/views/dashboard/strategydetails.html":
/*!******************************************************!*\
  !*** ./src/app/views/dashboard/strategydetails.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1>it works</h1>\r\n"

/***/ }),

/***/ "./src/app/views/dashboard/trade.ts":
/*!******************************************!*\
  !*** ./src/app/views/dashboard/trade.ts ***!
  \******************************************/
/*! exports provided: Trade */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Trade", function() { return Trade; });
var Trade = /** @class */ (function () {
    function Trade() {
    }
    return Trade;
}());



/***/ })

}]);
//# sourceMappingURL=views-dashboard-dashboard-module.js.map