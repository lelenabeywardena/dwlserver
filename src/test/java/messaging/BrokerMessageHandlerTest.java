package messaging;


import static org.junit.Assert.assertEquals;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.Test;

/**
 * @author Wei Du
 */

public class BrokerMessageHandlerTest {
	BrokerMessageHandler val = BrokerMessageHandler.getInstance();


    @Test
    public void buy() 
        throws Exception
    {    
    	String time = OffsetDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    	String result = ("<trade>\n" + 
        		"  <buy>true</buy>\n" + 
        		"  <id>3</id>\n" + 
        		"  <price>15.03</price>\n" + 
        		"  <size>5000</size>\n" + 
        		"  <stock>AAPL</stock>\n" + 
        		"  <whenAsDate>"+time+"</whenAsDate>\n" + 
        		"</trade>").split("\\.\\d{3}\\-")[0]; //regex to solve issue for miliseconds
    	System.out.println(result);
        assertEquals(result, val.buy(15.03, 5000, "AAPL", 3).split("\\.\\d{2,3}\\-")[0]);
    }
    
    @Test
    public void sell() 
        throws Exception
    {  
    	String time = OffsetDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    	String result = ("<trade>\n" + 
        		"  <buy>false</buy>\n" + 
        		"  <id>15</id>\n" + 
        		"  <price>118.0</price>\n" + 
        		"  <size>10000</size>\n" + 
        		"  <stock>C</stock>\n" + 
        		"  <whenAsDate>"+time+"</whenAsDate>\n" + 
        		"</trade>").split("\\.\\d{3}\\-")[0];
    	System.out.println(result);
        assertEquals(result, val.sell(118, 10000, "C", 15).split("\\.\\d{2,3}\\-")[0]);
    }
}
