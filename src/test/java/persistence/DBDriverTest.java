package persistence;

import java.util.ArrayList;

import org.junit.Assert;

import org.junit.Test;

import persistence.CRUDService.NotFoundException;

public class DBDriverTest {

	
	@Test
	public void getPerformanceest() {

		DBDriver dd = DBDriver.getInstance();
		try {
			dd.getPerformance();
		} finally {
			dd.close();
		}
	}
	
	@Test
	public void getByIDTest() {
		StrategyType t = new StrategyType("foobar");
		Strategy s = new Strategy("AAAA", 1, 2, 3, 4, t, 70001);
		DBDriver dd = DBDriver.getInstance();
		dd.insertStrategy(s);
		Strategy sbyfind;
		try {
			sbyfind = dd.getStrategy(s.getId());
			Assert.assertNotNull(sbyfind);
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			dd.close();
		}

	}

	@Test
	public void getTransactionsByStrategyID() {
		StrategyType t = new StrategyType("foobar");
		Strategy s = new Strategy("AAAA", 1, 2, 3, 4, t, 70001);
		DBDriver dd = DBDriver.getInstance();
		dd.insertStrategy(s);
		ArrayList<Transaction> ts;

		ts = dd.getTrades(s);
		Assert.assertNotNull(ts);

		dd.close();

	}

}
