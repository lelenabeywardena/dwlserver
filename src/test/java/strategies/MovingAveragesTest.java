package strategies;

import static org.junit.Assert.assertEquals;

import java.util.LinkedList;
import java.util.Queue;

import org.junit.Test;

import etcs.Price;

/**
 * @author Wei Du
 */

public class MovingAveragesTest {

    @Test
	public void calcMATest() throws Exception {
    	 Queue<Price> sampleData = new LinkedList<Price>();
    	    Price p = new Price("lol", 3, 4, 2.45, 5, 100);
    	    Price s = new Price("wut", 4, 5, 4.35, 6, 200);
    	    sampleData.add(p);
    	    sampleData.add(s);
    	    MovingAverages test = new MovingAverages(sampleData, 2);
    	    assertEquals((p.high+p.low+s.high+s.low)/4, test.calcMA(),0.001);
	}
    
    @Test(expected = IndexOutOfBoundsException.class)
	public void calcMATestError() throws Exception{
    	 Queue<Price> sampleData = new LinkedList<Price>();
    	    Price p = new Price("lol", 3, 4, 2.45, 5, 100);
    	    Price s = new Price("wut", 4, 5, 4.35, 6, 200);
    	    sampleData.add(p);
    	    sampleData.add(s);
    	    MovingAverages test = new MovingAverages(sampleData, 3);
			test.calcMA();
	}
}
