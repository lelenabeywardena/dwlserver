package strategies;

import org.junit.Test;
import org.junit.Assert;

import persistence.CRUDService.NotFoundException;
import persistence.DBDriver;
import persistence.Strategy;
import persistence.StrategyType;

/**
 * @author Wei Du
 */
public class StrategyRunnerTest {
	
	@Test
	public void enqueueTest() {
		StrategyType t = new StrategyType("foobar");
		Strategy s = new Strategy("AAAA", 1, 2, 3, 4, t, 70001);
		StrategyRunner.getInstance().enqueueStrategy(s);
		Assert.assertFalse(StrategyRunner.getInstance().getRunningStrategies().isEmpty());
	}
	
	@Test
	public void enqueueIntegrationTest() throws NotFoundException {
		StrategyType t = new StrategyType("foobar");
		Strategy s = new Strategy("AAAA", 1, 2, 3, 4, t, 70001);
		DBDriver dd = DBDriver.getInstance();
		dd.insertStrategy(s);
		Assert.assertNotNull(dd.getStrategy(s.getId()));
		dd.close();
		StrategyRunner.getInstance().enqueueStrategy(s);
		Assert.assertFalse(StrategyRunner.getInstance().getRunningStrategies().isEmpty());
	}
}
