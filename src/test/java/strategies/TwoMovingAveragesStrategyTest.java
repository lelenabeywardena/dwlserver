package strategies;

import static org.junit.Assert.assertEquals;

import java.util.LinkedList;
import java.util.Queue;

import org.junit.Test;

import etcs.Price;

/**
 * @author Wei Du
 */

public class TwoMovingAveragesStrategyTest {


	@Test
	public void setShortPricesTest() throws Exception { 
		Queue<Price> sampleData = new LinkedList<Price>();
	    Price p = new Price("lol", 3, 4, 2.45, 5, 100);
	    Price s = new Price("wut", 4, 5, 4.35, 6, 200);
	    sampleData.add(p);
	    sampleData.add(s);
		TwoMovingAveragesStrategy test = new TwoMovingAveragesStrategy();
		test.setShortPrices(sampleData, 2);
		assertEquals((p.high+p.low+s.high+s.low)/4,test.short_prices.calcMA(),0.001);
	}
	@Test
	public void setLongPricesTest() throws Exception { 
		Queue<Price> sampleData = new LinkedList<Price>();
	    Price p = new Price("lol", 3, 4, 2.45, 5, 100);
	    Price s = new Price("wut", 4, 5, 4.35, 6, 200);
	    sampleData.add(p);
	    sampleData.add(s);
		TwoMovingAveragesStrategy test = new TwoMovingAveragesStrategy();
		test.setLongPrices(sampleData, 2);
		assertEquals((p.high+p.low+s.high+s.low)/4,test.long_prices.calcMA(),0.001);
	}
	@Test
	public void make_decisionTest() throws Exception { 
		Queue<Price> sampleData = new LinkedList<Price>();
	    Price p = new Price("lol", 3, 4, 2.45, 5, 100);
	    Price s = new Price("wut", 4, 5, 4.35, 6, 200);
	    sampleData.add(p);
	    sampleData.add(s);
		TwoMovingAveragesStrategy test = new TwoMovingAveragesStrategy();
		test.setLongPrices(sampleData, 2);
		test.setShortPrices(sampleData, 1);
		assertEquals(strategies.Decision.SELL,test.make_decision(5));
		assertEquals(strategies.Decision.SELL,test.make_decision(6));
	}
}
